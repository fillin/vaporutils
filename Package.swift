// swift-tools-version: 5.8
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "VaporUtils",
    platforms: [
       .macOS(.v13)
    ],
    products: [
        .library(
            name: "VaporUtils",
            targets: ["VaporUtils"]),
    ],
    dependencies: [
        .package(url: "https://github.com/vapor/vapor.git", from: "4.77.1"),
    ],
    targets: [
        .target(
            name: "VaporUtils",
            dependencies: [
                .product(name: "Vapor", package: "vapor"),
            ]
        ),
    ]
)
