import Vapor
import NIOHTTP1

public struct APIResponse<T> : Content where T: Content {
    
    public var data: T
    
    public init(data: T) {
        self.data = data
    }
}

public struct BlankResponse: Content {
    public init() {}
}

public struct APIError: AbortError, DebuggableError {
    /// See `Debuggable`
    public var identifier: String

    /// See `AbortError`
    public var status: HTTPResponseStatus

    /// See `AbortError`.
    public var headers: HTTPHeaders

    /// See `AbortError`
    public var reason: String

    /// Source location where this error was created.
    public var source: ErrorSource?

    /// Create a new `Abort`, capturing current source location info.
    public init(
        _ status: HTTPResponseStatus,
        headers: HTTPHeaders = [:],
        reason: String? = nil,
        identifier: String? = nil,
        suggestedFixes: [String] = [],
        file: String = #fileID,
        function: String = #function,
        line: UInt = #line,
        column: UInt = #column,
        range: Range<UInt>? = nil
    ) {
        self.identifier = identifier ?? status.code.description
        self.headers = headers
        self.status = status
        self.reason = reason ?? status.reasonPhrase
        self.source = ErrorSource(
            file: file,
            function: function,
            line: line,
            column: column,
            range: range
        )
    }
}
