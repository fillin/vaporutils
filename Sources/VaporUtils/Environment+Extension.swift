import Vapor

extension Environment {
    public static func get<T>(_ key: String) -> T? {
        guard let value = self.get(key) else {
            return nil
        }
        if T.self == Int.self {
            return Int(value) as? T
        } else if T.self == Double.self {
            return Double(value) as? T
        } else if T.self == TimeInterval.self {
            return TimeInterval(value) as? T
        } else if T.self == Bool.self {
            return value.boolValue as? T
        } else {
            return value as? T
        }
    }
    
    public static func get<T>(_ key: String, default: T) -> T {
        if let value: T = self.get(key) {
            return value
        }
        return `default`
    }
    
    public static func get<T: RawRepresentable>(_ key: String, default: T) -> T {
        if let value: T = self.get(key) {
            return value
        }
        guard let value = self.get(key) as? T.RawValue else {
            return `default`
        }
        if let result = T(rawValue: value) {
            return result
        }
        return `default`
    }
}

