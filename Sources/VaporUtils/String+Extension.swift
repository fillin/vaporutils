import Foundation

extension String {
    public var boolValue: Bool {
        switch self.lowercased() {
        case "true","yes": return true
        case "false","no": return false
        case "0": return false
        case "1": return true
        default: return false
        }
    }
}
