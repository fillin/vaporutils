import Foundation

public final class ShellCommand: CustomStringConvertible {
    
    public struct Params {
        public let path: String
        public let args: [String]
    }
    
    let params: Params
    
    public init(binPath: String, args: [String]) {
        self.params = Params(path: binPath, args: args)
    }
    
    public init(_ params: Params) {
        self.params = params
    }

    @discardableResult
    public func run(_ terminationHandler: ((Process) -> Void)? = nil) throws -> String {
        guard FileManager.default.fileExists(atPath: params.path) else {
            throw RuntimeError("No file for 'Binary path' at \(params.path)")
        }
        
        let task = Process()
        let pipe = Pipe()
        
        task.standardOutput = pipe
        task.standardError = pipe
        task.arguments = params.args
        task.executableURL = URL(fileURLWithPath: params.path)
        task.standardInput = nil
        task.terminationHandler = terminationHandler
        try task.run()
        
        let data = pipe.fileHandleForReading.readDataToEndOfFile()
        guard let output = String(data: data, encoding: .utf8) else {
            throw RuntimeError("Shell: Failed to read command output")
        }

        if !task.isRunning, task.terminationStatus != .zero {
            throw RuntimeError("Process exited with status: \(task.terminationStatus). Reason: \(task.terminationReason)")
        }
        
        return output
    }
    
    @discardableResult
    public func run() async throws -> String {
        var isTermiated = false
        let result = try run { _ in
            isTermiated = true
        }
        while !isTermiated {
            try await Task.sleep(for: .milliseconds(10))
        }
        return result
    }
    
    public var description: String {
        "\(params.path) \(params.args.joined(separator: " "))"
    }
}
