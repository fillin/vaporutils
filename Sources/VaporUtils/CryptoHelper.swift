import Foundation
import Crypto

public extension String {
    
    func aesEncrypted(_ key: SymmetricKey, encoding: String.Encoding) throws -> Data {
        try self.data(by: encoding).aesEncrypted(key)
    }
    
    func aesEncryptedString(_ key: SymmetricKey, encoding: String.Encoding) throws -> String {
        try self.aesEncrypted(key, encoding: encoding).base64EncodedString()
    }
    
    func aesDecrypted(_ key: SymmetricKey) throws -> Data {
        guard let data = Data(base64Encoded: self) else {
            throw EncryptionError.invalidBase64Data
        }
        return try data.aesDecrypted(key)
    }
    
    func aesDecryptedString(_ key: SymmetricKey, encoding: String.Encoding) throws -> String {
        try self.aesDecrypted(key).string(with: encoding)
    }
}

public extension String {
    func data(by encoding: String.Encoding) throws -> Data {
        guard let data = self.data(using: encoding) else {
            throw EncryptionError.stringEncodingFailed
        }
        return data
    }
    
    init(someData data: Data, encoding: String.Encoding) throws {
        guard let string = String(data: data, encoding: encoding) else {
            throw EncryptionError.stringEncodingFailed
        }
        self = string
    }
}

public extension Data {
    func string(with encoding: String.Encoding) throws -> String {
        try String(someData: self, encoding: encoding)
    }
}

public extension Data {

    func aesEncrypted(_ key: SymmetricKey) throws -> Data {
        let sealedBox = try AES.GCM.seal(self, using: key)
        guard let encryptedData = sealedBox.combined else {
            throw EncryptionError.sealedBoxFailure
        }
        return encryptedData
    }

    func aesDecrypted(_ key: SymmetricKey) throws -> Data {
        let newBox = try AES.GCM.SealedBox(combined: self)
        return try AES.GCM.open(newBox, using: key)
    }
}

public extension SymmetricKey {
    
    init(string: String, keySize: Int = 24) throws {
        guard var keyData = string.data(using: .utf8)?.base64EncodedData() else {
            throw EncryptionError.symmetricKeyFailed
        }
        if keyData.count > keySize {
            keyData = keyData.subdata(in: 0 ..< keySize)
        }
        self = SymmetricKey(data: keyData)
    }
}

public extension SymmetricKey {

    /// Creates a `SymmetricKey` from a Base64-encoded `String`.
    ///
    /// - Parameter base64EncodedString: The Base64-encoded string from which to generate the `SymmetricKey`.
    init?(base64EncodedString: String) {
        guard let data = Data(base64Encoded: base64EncodedString) else {
            return nil
        }
        self.init(data: data)
    }

    // MARK: - Instance Methods

    /// Serializes a `SymmetricKey` to a Base64-encoded `String`.
    func serialized() -> String {
        self.withUnsafeBytes { body in
            Data(body).base64EncodedString()
        }
    }
}

public extension Data {
    func sha256hex() -> String {
        SHA256.hash(data: self).hex
    }
}

public extension Data {
    init?(hex: String) {
        guard hex.count.isMultiple(of: 2) else {
            return nil
        }
        let chars = hex.map { $0 }
        let bytes = stride(from: 0, to: chars.count, by: 2)
            .map { String(chars[$0]) + String(chars[$0 + 1]) }
            .compactMap { UInt8($0, radix: 16) }
        
        guard hex.count / bytes.count == 2 else { return nil }
        self.init(bytes)
    }
}

public enum EncryptionError: Error {
    case encryptionFailure
    case sealedBoxFailure
    case symmetricKeyFailed
    case invalidBase64Data
    case stringEncodingFailed
}
